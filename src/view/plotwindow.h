#ifndef PLOTWINDOW_H
#define PLOTWINDOW_H

#include <QWidget>

namespace Ui {
class PlotWindow;
}

namespace s21 {
class PlotWindow : public QWidget {
  Q_OBJECT

 public:
  explicit PlotWindow(QWidget *parent = nullptr);
  ~PlotWindow();

  void DrawPlot(std::vector<double> error_data);

 private:
  Ui::PlotWindow *ui;
};
}  // namespace s21

#endif  // PLOTWINDOW_H
