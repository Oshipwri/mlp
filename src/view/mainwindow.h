#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QMainWindow>
#include <QPixmap>
#include <QResizeEvent>
#include <QThread>
#include <QTimer>

#include "../controller/controller.h"
#include "drawarea.h"
#include "plotwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

namespace s21 {

class AsyncWorker : public QObject {
  Q_OBJECT
 public:
  explicit AsyncWorker(s21::Controller *controller, QObject *parent = nullptr)
      : controller_(controller) {}
  ~AsyncWorker(){};
  QString error = "";

 public slots:
  void RunLearnRandomWeights();
  void RunTest();
  void RunCrossValid();

 signals:
  void learn_finished();
  void test_finished();
  void cross_valid_finished();

 private:
  s21::Controller *controller_;
};

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(s21::Controller *controller, QWidget *parent = nullptr);
  ~MainWindow();

  void DisableButtons();
  void EnableButtons();

 private:
  Ui::MainWindow *ui;
  DrawArea *draw_area;
  QTimer *timer;
  QThread *thread;
  Controller *controller_;
  AsyncWorker *worker;
  PlotWindow plotwindow;
  void resizeEvent(QResizeEvent *event);
  void SetPerceptron();
  static constexpr int kMaxImageSize = 512;

 private slots:
  void slotTimer();
  void on_clear_button_clicked();
  void on_load_image_button_clicked();
  void on_predict_button_clicked();
  void on_learn_random_button_clicked();
  void on_load_weights_button_clicked();
  void on_save_weights_button_clicked();
  void on_load_learn_sample_button_clicked();
  void on_load_test_sample_button_clicked();
  void on_make_test_button_clicked();
  void on_reset_weights_button_clicked();
  void on_make_cross_valid_button_clicked();
  void on_show_error_plot_button_clicked();

  void ReadyToLearn();
  void ReadyToTest();
  void ReadyToCrossValid();

 signals:
  void start_learn();
  void start_test();
  void start_cross_valid();
};
}  // namespace s21
#endif  // MAINWINDOW_H
