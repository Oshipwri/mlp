#include "drawarea.h"

namespace s21 {

DrawArea::DrawArea(QObject *parent) : QGraphicsScene(parent) {}

DrawArea::~DrawArea() {}

void DrawArea::mousePressEvent(QGraphicsSceneMouseEvent *event) {
  addEllipse(event->scenePos().x(), event->scenePos().y(), kBrushSize,
             kBrushSize, QPen(Qt::NoPen), QBrush(Qt::black));
  previousPoint = event->scenePos();
}

void DrawArea::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
  addLine(previousPoint.x(), previousPoint.y(), event->scenePos().x(),
          event->scenePos().y(),
          QPen(Qt::black, kBrushSize, Qt::SolidLine, Qt::RoundCap));
  previousPoint = event->scenePos();
}

}  // namespace s21
