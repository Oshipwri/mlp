#ifndef DRAWAREA_H
#define DRAWAREA_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

namespace s21 {
class DrawArea : public QGraphicsScene {
 public:
  explicit DrawArea(QObject *parent = nullptr);
  ~DrawArea();

 private:
  QPointF previousPoint;

  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

  static constexpr int kBrushSize = 10;
};
}  // namespace s21

#endif  // DRAWAREA_H
