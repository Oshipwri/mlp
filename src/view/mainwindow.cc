#include "mainwindow.h"

#include "ui_mainwindow.h"

namespace s21 {

MainWindow::MainWindow(s21::Controller *controller, QWidget *parent)
    : controller_(controller), QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  draw_area = new DrawArea();
  ui->draw_area->setScene(draw_area);

  timer = new QTimer();
  connect(timer, &QTimer::timeout, this, &MainWindow::slotTimer);

  thread = new QThread(this);
  connect(this, SIGNAL(destroyed()), thread, SLOT(quit()));

  
  worker = new AsyncWorker(controller);
  connect(this, SIGNAL(start_learn()), worker, SLOT(RunLearnRandomWeights()));
  connect(this, SIGNAL(start_test()), worker, SLOT(RunTest()));
  connect(this, SIGNAL(start_cross_valid()), worker, SLOT(RunCrossValid()));
  worker->moveToThread(thread);
  thread->start();

  connect(worker, SIGNAL(learn_finished()), this, SLOT(ReadyToLearn()));
  connect(worker, SIGNAL(test_finished()), this, SLOT(ReadyToTest()));
  connect(worker, SIGNAL(cross_valid_finished()), SLOT(ReadyToCrossValid()));
}

MainWindow::~MainWindow() {
  delete thread;
  delete ui;
  delete worker;
  delete draw_area;
  delete timer;
}

void MainWindow::slotTimer() {
  timer->stop();
  draw_area->setSceneRect(0, 0, ui->draw_area->width() - 10,
                          ui->draw_area->height() - 10);
}

void MainWindow::resizeEvent(QResizeEvent *event) {
  timer->start(100);
  QWidget::resizeEvent(event);
}

void MainWindow::on_clear_button_clicked() { draw_area->clear(); }

void MainWindow::on_load_image_button_clicked() {
  QString q_filename = QFileDialog::getOpenFileName(this, "Select file", ".",
                                                    "bmp files (*.bmp)");
  if (!q_filename.isEmpty()) {
    try {
      QPixmap qp = QPixmap(q_filename);
      if (qp.size().width() > kMaxImageSize ||
          qp.size().height() > kMaxImageSize) {
        throw std::out_of_range("Incorrect file");
      }
      qp = qp.scaled(QSize(280, 280), Qt::IgnoreAspectRatio,
                     Qt::SmoothTransformation);
      //    QGraphicsPixmapItem item(qp);
      draw_area->clear();
      draw_area->addPixmap(qp);
    } catch (...) {
      ui->statusbar->showMessage("Incorrect image");
    }
  }
}

void MainWindow::on_predict_button_clicked() {
  ui->character_label->setText("A");
  QPixmap pixMap = this->ui->draw_area->grab();
  pixMap = pixMap.scaled(QSize(28, 28), Qt::KeepAspectRatio,
                         Qt::SmoothTransformation);
  QImage image = pixMap.toImage();
  std::vector<int> output;
  for (int i = 0; i < image.width(); i++) {
    for (int j = 0; j < image.height(); j++) {
      output.push_back(qGray(image.pixel(i, j)));
    }
  }
  char predicted_letter = controller_->Predict(output);
  ui->character_label->setText(QString(QChar(predicted_letter)));
}

void MainWindow::on_learn_random_button_clicked() {
  DisableButtons();
  SetPerceptron();
  ui->statusbar->clearMessage();
  controller_->SetTestFilePercent(ui->percent_spin->value());
  controller_->SetEpochs(ui->epochs_number_spin->value());
  controller_->SetNumberOfHiddenLayers(ui->layers_number_spin->value());
  emit start_learn();
}

void MainWindow::on_make_test_button_clicked() {
  DisableButtons();
  ui->statusbar->clearMessage();
  controller_->SetTestFilePercent(ui->percent_spin->value());
  emit start_test();
}

void MainWindow::on_make_cross_valid_button_clicked() {
  DisableButtons();
  ui->statusbar->clearMessage();
  ui->cross_valid_output_area->clear();
  SetPerceptron();
  controller_->SetTestFilePercent(1);
  controller_->SetGroupsNumber(ui->groups_number_spin->value());
  controller_->SetNumberOfHiddenLayers(ui->layers_number_spin->value());
  controller_->SetCrossValidLearnFileName(controller_->GetPathToLearnFile());
  emit start_cross_valid();
}

void MainWindow::on_load_weights_button_clicked() {
  QString q_filename = QFileDialog::getOpenFileName(this, "Select file", ".",
                                                    "txt files (*.txt)");
  if (!q_filename.isEmpty()) {
    try {
      SetPerceptron();
      controller_->LoadWeights(q_filename.toStdString());
      ui->layers_number_spin->setValue(controller_->GetNumberOfHiddenLayers());
      ui->statusbar->clearMessage();
    } catch (const std::exception &ex) {
      ui->statusbar->showMessage(ex.what());
    }
  }
}

void MainWindow::on_save_weights_button_clicked() {
  QString q_filename =
      QFileDialog::getSaveFileName(0, "Save as...", "", "TXT (*.txt)");
  if (q_filename.size() > 1) {
    controller_->SaveWeights(q_filename.toStdString());
  }
}

void MainWindow::on_load_learn_sample_button_clicked() {
  QString q_filename = QFileDialog::getOpenFileName(this, "Select file", ".",
                                                    "csv files (*.csv)");
  if (!q_filename.isEmpty()) {
    controller_->SetLearnFile(q_filename.toStdString());
    ui->load_learn_sample_label->setText(
        q_filename.split(u'/').value(q_filename.split(u'/').length() - 1));
  }
}

void MainWindow::on_load_test_sample_button_clicked() {
  QString q_filename = QFileDialog::getOpenFileName(this, "Select file", ".",
                                                    "csv files (*.csv)");
  if (!q_filename.isEmpty()) {
    controller_->SetTestFile(q_filename.toStdString());
    ui->load_test_sample_label->setText(
        q_filename.split(u'/').value(q_filename.split(u'/').length() - 1));
  }
}

void MainWindow::DisableButtons() {
  ui->make_test_button->setDisabled(true);
  ui->learn_random_button->setDisabled(true);
  ui->reset_weights_button->setDisabled(true);
  ui->make_cross_valid_button->setDisabled(true);
  ui->show_error_plot_button->setDisabled(true);
  ui->predict_button->setDisabled(true);
  ui->load_learn_sample_button->setDisabled(true);
  ui->load_test_sample_button->setDisabled(true);
  ui->save_weights_button->setDisabled(true);
  ui->load_weights_button->setDisabled(true);
}

void MainWindow::EnableButtons() {
  ui->learn_random_button->setEnabled(true);
  ui->make_test_button->setEnabled(true);
  ui->reset_weights_button->setEnabled(true);
  ui->make_cross_valid_button->setEnabled(true);
  ui->show_error_plot_button->setEnabled(true);
  ui->predict_button->setEnabled(true);
  ui->load_learn_sample_button->setEnabled(true);
  ui->load_test_sample_button->setEnabled(true);
  ui->save_weights_button->setEnabled(true);
  ui->load_weights_button->setEnabled(true);
}

void MainWindow::on_reset_weights_button_clicked() {
  SetPerceptron();
  ui->statusbar->clearMessage();
  controller_->SetNumberOfHiddenLayers(ui->layers_number_spin->value());
}

void MainWindow::on_show_error_plot_button_clicked() {
  ui->statusbar->clearMessage();
  if (controller_->GetErrorData().size() > 1) {
    plotwindow.DrawPlot(controller_->GetErrorData());
    plotwindow.show();
  } else {
    ui->statusbar->showMessage("Not enough data to create plot");
  }
}

void MainWindow::SetPerceptron() {
  if (ui->metod_combo_box->currentText() == "matrix method")
    controller_->SetMatrixMethod();
  else if (ui->metod_combo_box->currentText() == "graph method")
    controller_->SetGraphMethod();
}

void MainWindow::ReadyToLearn() {
  ui->statusbar->showMessage(worker->error);
  EnableButtons();
}

void MainWindow::ReadyToTest() {
  EnableButtons();
  ui->statusbar->showMessage(worker->error);
  if (!worker->error.size()) {
    ui->accuracy_result_label->setText(
        QString::number(controller_->GetAccuracy()));
    ui->total_time_result_label->setText(
        QString::number(controller_->GetTestTime()));
    ui->precision_result_label->setText(
        QString::number(controller_->GetPrecision()));
    ui->f_measure_result_label->setText(
        QString::number(controller_->GetPrecision()));
    ui->recall_result_label->setText(
        QString::number(controller_->GetPrecision()));
  } else {
    ui->accuracy_result_label->clear();
    ui->total_time_result_label->clear();
    ui->precision_result_label->clear();
    ui->f_measure_result_label->clear();
    ui->recall_result_label->clear();
  }
}

void MainWindow::ReadyToCrossValid() {
  EnableButtons();
  ui->statusbar->showMessage(worker->error);
  if (!worker->error.size())
    ui->cross_valid_output_area->setText(
        QString::fromStdString(controller_->GetCrossValidOutput()));
  else
    ui->cross_valid_output_area->clear();
}

void AsyncWorker::RunLearnRandomWeights() {
  error = "";
  try {
    controller_->LearnRandomWeights();
  } catch (const std::exception &ex) {
    error = ex.what();
  }
  emit learn_finished();
}

void AsyncWorker::RunTest() {
  error = "";
  try {
    controller_->MakeTest();
  } catch (const std::exception &ex) {
    error = ex.what();
  }
  emit test_finished();
}

void AsyncWorker::RunCrossValid() {
  error = "";
  try {
    controller_->CrossValid();
  } catch (const std::exception &ex) {
    error = ex.what();
  }
  emit cross_valid_finished();
}

}  // namespace s21
