#include "plotwindow.h"

#include "ui_plotwindow.h"

namespace s21 {
PlotWindow::PlotWindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::PlotWindow) {
  ui->setupUi(this);
}

void PlotWindow::DrawPlot(std::vector<double> error_data) {
  ui->plot_error->clearGraphs();
  QVector<double> x_coordinates(error_data.size()),
      y_coordinates(error_data.size());

  for (int i = 0; i < error_data.size(); i++) {
    x_coordinates[i] = i + 1;
    y_coordinates[i] = error_data[i];
  }
  ui->plot_error->addGraph();
  ui->plot_error->graph(0)->setData(x_coordinates, y_coordinates);
  ui->plot_error->xAxis->setLabel("Epoch");
  ui->plot_error->yAxis->setLabel("Percent of correct answers");

  ui->plot_error->xAxis->setRange(0, error_data.size() + 1);
  ui->plot_error->yAxis->setRange(-1, 100);
  ui->plot_error->replot();
}

PlotWindow::~PlotWindow() { delete ui; }
}  // namespace s21
