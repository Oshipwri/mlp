#ifndef SRC_MODEL_BASE_H
#define SRC_MODEL_BASE_H

#include <array>

namespace s21 {
class Base {
 public:
  Base(){};
  ~Base(){};
  Base(const Base &other) = default;
  Base(Base &&other) = default;
  Base &operator=(const Base &other) = delete;
  Base &operator=(Base &&other) = delete;

  static const int kInputNeuronsNumber = 784;  // кол-во нейронов в входном слое
  static const int kNeuronHValue = 128;  // кол-во нейронов в скрытых слоях
  static const int kOutputNeuronsNumber =
      26;  // кол-во нейронов в выходном слое
  double learn_step = 0.1;
  std::array<int, 6> DefaultValueNeurons = {784, 128, 96, 64, 48, 26};
  using income_t = std::array<double, kInputNeuronsNumber>;
  using result_t = std::array<double, kOutputNeuronsNumber>;
};

}  // namespace s21

#endif  // SRC_MODEL_BASE_H
