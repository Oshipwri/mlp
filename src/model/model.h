#ifndef SRC_MODEL_MODEL_H
#define SRC_MODEL_MODEL_H

#include "./graph_model/graph_model.h"
#include "./matrix_model/matrix_model.h"

namespace s21 {

class Model;

class Strategy {
 protected:
 public:
  virtual ~Strategy(){};

  virtual void Learn() = 0;
  virtual void MakeTest() = 0;
  virtual char Predict(std::vector<int> &neurons) = 0;
  virtual int GetNumberOfHiddenLayers() = 0;
  virtual void SetNumberOfHiddenLayers(int layers_number) = 0;
  virtual void SetEpochs(int epochs_number) = 0;
  virtual void SetGroups(int groups_number) = 0;
  virtual void SaveWeights(const std::string &path) = 0;
  virtual double GetAccuracy() = 0;
  virtual double GetTestTime() = 0;
  virtual double GetPrecision() = 0;
  virtual void SetCrossValidLearnFileName(const std::string &path) = 0;
  virtual void CrossValid() = 0;
  virtual std::string GetCrossValidOutput() = 0;
  virtual std::vector<double> GetError() = 0;
};

class MatrixStrategy : public Strategy {
 private:
  Model *model_;

 public:
  explicit MatrixStrategy(Model *model) : model_(model) {}
  ~MatrixStrategy() override;
  MatrixStrategy(const MatrixStrategy &other) = default;
  MatrixStrategy(MatrixStrategy &&other) = default;

  MatrixStrategy &operator=(const MatrixStrategy &other) = delete;
  MatrixStrategy &operator=(MatrixStrategy &&other) = delete;
  void Learn() override;
  void MakeTest() override;
  char Predict(std::vector<int> &neurons) override;
  int GetNumberOfHiddenLayers() override;
  void SetNumberOfHiddenLayers(int layers_number) override;
  void SetEpochs(int epochs_number) override;
  void SetGroups(int groups_number) override;
  void SaveWeights(const std::string &path) override;
  double GetAccuracy() override;
  double GetTestTime() override;
  double GetPrecision() override;
  void SetCrossValidLearnFileName(const std::string &path) override;
  void CrossValid() override;
  std::string GetCrossValidOutput() override;
  std::vector<double> GetError() override;
};

class GraphStrategy : public Strategy {
 private:
  Model *model_;

 public:
  explicit GraphStrategy(Model *model) : model_(model) {}
  ~GraphStrategy() override;
  GraphStrategy(const GraphStrategy &other) = default;
  GraphStrategy(GraphStrategy &&other) = default;
  GraphStrategy &operator=(const GraphStrategy &other) = delete;
  GraphStrategy &operator=(GraphStrategy &&other) = delete;

  void Learn() override;
  void MakeTest() override;
  char Predict(std::vector<int> &neurons) override;
  int GetNumberOfHiddenLayers() override;
  void SetNumberOfHiddenLayers(int layers_number) override;
  void SetEpochs(int epochs_number) override;
  void SetGroups(int groups_number) override;
  void SaveWeights(const std::string &path) override;
  double GetAccuracy() override;
  double GetTestTime() override;
  double GetPrecision() override;
  void SetCrossValidLearnFileName(const std::string &path) override;
  void CrossValid() override;
  std::string GetCrossValidOutput() override;
  std::vector<double> GetError() override;
};

class Model {
 public:
  Model();
  ~Model() {}

  Model(const Model &other) = default;
  Model(Model &&other) = default;

  Model &operator=(const Model &other) = delete;
  Model &operator=(Model &&other) = delete;

  void Learn();
  void MatrixModelLearn();
  void GraphModelLearn();

  void MakeTest();
  void MatrixModelMakeTest();
  void GraphModelMakeTest();

  void CrossValid();
  void MatrixModelCrossValid();
  void GraphModelCrossValid();

  char Predict(std::vector<int> &neurons);
  char MatrixModelPredict(const std::vector<int> &neurons);
  char GraphModelPredict(const std::vector<int> &neurons);

  void LoadWeights(const std::string &path);

  void SaveWeights(const std::string &path);
  void SaveWeightsInMatrixModel(const std::string &path);
  void SaveWeightsInGraphModel(const std::string &path);

  int GetNumberOfHiddenLayers();
  int GetNumberOfHiddenLayersInMatrixModel();
  int GetNumberOfHiddenLayersInGraphModel();

  void SetNumberOfHiddenLayers(int layers_number);
  void SetNumberOfHiddenLayersInMatrixModel(int layers_number);
  void SetNumberOfHiddenLayersInGraphModel(int layers_number);

  void SetEpochs(int epochs_number);
  void SetEpochsInMatrixModel(int epochs_number);
  void SetEpochsInGraphModel(int epochs_number);

  void SetGroupsNumber(int groups_number);
  void SetGroupsInMatrixModel(int groups_number);
  void SetGroupsInGraphModel(int groups_number);

  void SetLearnFile(const std::string &path);
  void SetTestFile(const std::string &path);

  void SetTestFilePercent(double percent);

  double GetAccuracy();
  double GetAccuracyInMatrixModel();
  double GetAccuracyInGraphModel();

  double GetPrecision();
  double GetPrecisionInMatrixModel();
  double GetPrecisionInGraphModel();

  double GetTestTime();
  double GetTestTimeInMatrixModel();
  double GetTestTimeInGraphModel();

  std::vector<double> GetErrorData();
  std::vector<double> GetErrorDataInMatrixModel();
  std::vector<double> GetErrorDataInGraphModel();

  std::string GetPathToLearnFile();

  void SetCrossValidLearnFileName(const std::string &path);
  void SetCrossValidLearnFileInMatrixModel(const std::string &path);
  void SetCrossValidLearnFileInGraphModel(const std::string &path);

  std::string GetCrossValidOutput();
  std::string GetCrossValidOutputInMatrixModel();
  std::string GetCrossValidOutputInGraphModel();

  void SetMatrixStrategy();
  void SetGraphStrategy();

 private:
  MatrixModel matrix_model_;
  GraphPreceptron graph_model_;
  MatrixStrategy matrix_strategy_;
  GraphStrategy graph_strategy_;
  Strategy *strategy_;
};

}  // namespace s21

#endif  // SRC_MODEL_MODEL_H
