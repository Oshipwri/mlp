#ifndef SRC_MODEL_MATRIX_MODEL_WEIGHTS_READER_WRITER_H_
#define SRC_MODEL_MATRIX_MODEL_WEIGHTS_READER_WRITER_H_

#include <fstream>
#include <iostream>
#include <vector>

#include "../base.h"
#include "../matrix/s21_matrix_oop.h"

namespace s21 {
class WeightsReaderWriter : public Base {
 public:
  WeightsReaderWriter(){};
  ~WeightsReaderWriter(){};

  void ReadFile(const std::string &filepath, std::vector<S21Matrix> &weights);
  void WriteFile(const std::string &filepath, S21Matrix &matrix);
  void WriteFile(const std::string &filepath, std::vector<S21Matrix> &weights);

 private:
  void WriteMatrix(const std::string &filepath, S21Matrix &matrix);
  void ReadMatrix(std::fstream *data, S21Matrix &matrix,
                  int hidden_layer_number, int current_hidden_layers_number);
};
}  // namespace s21
#endif  // SRC_MODEL_MATRIX_MODEL_WEIGHTS_READER_WRITER_H_
