#include <stdio.h>

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "../abstract_perceptron.h"
#include "../matrix/s21_matrix_oop.h"
#include "weights_reader_writer.h"

namespace s21 {

class MatrixModel : public BaseMLP {
 private:
  std::vector<S21Matrix> neurons_;
  WeightsReaderWriter wrw;
  std::vector<S21Matrix> weights_;
  std::vector<S21Matrix> weights_delta_;
  std::vector<S21Matrix> neuron_err_;
  std::vector<int> NumberOfNeuronsInLayer = {};
  S21Matrix OutputMatrix;  // матрица (kOutputNeuronsNumber, 1) ОЖИДАЕМОЕ
                           // значение на выходе из сети
  S21Matrix OutputMatrixLost;  // матрица (kOutputNeuronsNumber, 1) РЕАЛЬНОЕ
                               // значение на выходе
  int number_of_hidden_layers_ = 2;
  int correct_result_ = -1;
  int number_of_training = 0;
  int number_of_testing = 0;
  int correctly_predicted = 0;
  double average_accuracy = 0;
  double precision_of_prediction = 0;
  char ReturnLetter();
  char CompareResult();
  void InitNeurons();
  void InitWeights();
  void UpdateWeights(int layer);
  void LossFunction();
  void ActivateFunction(int current_layers);
  void TrainModel(const std::string& source_data);
  void CalcError(int layer);
  void MoveForward();
  void CalcWeightsDelta(int layers);
  double GetDerivative(double value);
  double NormalizeWithSigmoid(double value);

 public:
  MatrixModel();
  ~MatrixModel();
  char PredictFromString(const std::string& source_data);
  char Predict(std::vector<int> source_data);
  int GetNumberOfHiddenLayers();
  int GetNumberOfTraining();
  double GetAverageAccuracy();
  double GetPrecisionOfPrediction();
  void TestMode();
  void CrossValid();
  void InitMatrixModel();
  void StartProcessing();
  void LoadWeights(const std::string& source_data);
  void SaveWeights(const std::string& source_data);
  void ParseString(std::string set_of_letter);
  void SetNumberOfHiddenLayers(int value);
  void SetNumberOfNeuronsInLayer(int hidden_layers);
  void SetPathToData(const std::string& path, int mode);
  std::string GetCrossValidOut();
  std::string GetPathToDataTest();
  std::string GetPathToDataTrain();
};

}  // namespace s21
