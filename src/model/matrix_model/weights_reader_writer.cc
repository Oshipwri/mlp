#include "weights_reader_writer.h"
namespace s21 {
void WeightsReaderWriter::WriteMatrix(const std::string &filepath,
                                      S21Matrix &matrix) {
  std::ofstream out;
  out.open(filepath, std::ios::app);
  for (int i = 0; i < matrix.get_rows(); i++) {
    for (int j = 0; j < matrix.get_cols(); j++) {
      out << matrix(i, j);
      if (j != matrix.get_cols() - 1) out << " ";
    }
    out << std::endl;
  }
  out.close();
}

void WeightsReaderWriter::WriteFile(const std::string &filepath,
                                    S21Matrix &matrix) {
  std::ofstream out;
  out.open(filepath, std::ios::app);
  out << matrix.get_rows() << " " << matrix.get_cols() << std::endl;
  out.close();
  WriteMatrix(filepath, matrix);
}

void WeightsReaderWriter::WriteFile(const std::string &filepath,
                                    std::vector<S21Matrix> &weights) {
  std::ofstream out;
  out.open(filepath);
  out << weights.size();
  out << std::endl;
  out.close();
  for (int i = 0; i < weights.size(); i++) {
    WriteFile(filepath, weights[i]);
    if (i < weights.size() - 1) {
      out.open(filepath, std::ios::app);
      out << std::endl;
      out.close();
    }
  }
}

void WeightsReaderWriter::ReadMatrix(std::fstream *data, S21Matrix &matrix,
                                     int hidden_layer_number,
                                     int current_hidden_layers_number) {
  double current;
  *data >> current;
  if (hidden_layer_number == current_hidden_layers_number - 1) {
    if (current != DefaultValueNeurons[DefaultValueNeurons.size() - 1])
      throw std::out_of_range("Incorrect weights number");
  } else if (current != DefaultValueNeurons[hidden_layer_number + 1]) {
    throw std::out_of_range("Incorrect weights number");
  }
  matrix.set_rows(current);
  *data >> current;
  if (current != DefaultValueNeurons[hidden_layer_number])
    throw std::out_of_range("Incorrect number of neurons");
  matrix.set_cols(current);
  for (int i = 0; i < matrix.get_rows(); i++) {
    for (int j = 0; j < matrix.get_cols(); j++) {
      *data >> current;
      matrix(i, j) = current;
    }
  }
}

void WeightsReaderWriter::ReadFile(const std::string &filepath,
                                   std::vector<S21Matrix> &weights) {
  weights.clear();
  std::fstream data(filepath, std::ios_base::in);
  double current;
  data >> current;
  if (current < 2 || current > 5)
    throw std::out_of_range("Incorrect number of hidden layers");
  int current_hidden_layers_number = current;
  for (int i = 0; i < current; i++) {
    S21Matrix current_matrix;
    ReadMatrix(&data, current_matrix, i, current_hidden_layers_number);
    weights.push_back(current_matrix);
  }
  data.close();
}
}  // namespace s21
