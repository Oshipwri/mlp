#include "./matrix_model.h"

namespace s21 {

MatrixModel::MatrixModel() { InitMatrixModel(); }

MatrixModel::~MatrixModel() {}

void MatrixModel::SetNumberOfHiddenLayers(int value) {
  number_of_hidden_layers_ = value;
  NumberOfNeuronsInLayer.clear();
  SetNumberOfNeuronsInLayer(value);
  InitNeurons();
  InitWeights();
}

void MatrixModel::SetNumberOfNeuronsInLayer(int hidden_layers) {
  if (hidden_layers > 5 || hidden_layers < 2) {
    throw std::out_of_range("incorrect number of hidden layers");
  }
  for (int i = 0; i < hidden_layers; i++) {
    NumberOfNeuronsInLayer.push_back(DefaultValueNeurons[i]);
  }
  NumberOfNeuronsInLayer.push_back(
      DefaultValueNeurons[DefaultValueNeurons.size() - 1]);
}

int MatrixModel::GetNumberOfHiddenLayers() { return number_of_hidden_layers_; }

void MatrixModel::InitMatrixModel() {
  InitLearnStep();
  SetNumberOfHiddenLayers(4);
  InitNeurons();
  InitWeights();
}

void MatrixModel::InitNeurons() {
  neurons_.clear();
  for (int layer = number_of_hidden_layers_; layer >= 0; layer--) {
    auto it = neurons_.begin();
    neurons_.insert(it, S21Matrix(NumberOfNeuronsInLayer[layer], 1));
  }
}

void MatrixModel::InitWeights() {
  srand(time(NULL));
  number_of_hidden_layers_ = NumberOfNeuronsInLayer.size() - 1;
  weights_.clear();
  for (int layer = number_of_hidden_layers_; layer > 0; layer--) {
    auto it = weights_.begin();
    S21Matrix current_weights(NumberOfNeuronsInLayer[layer],
                              NumberOfNeuronsInLayer[layer - 1]);
    current_weights.fill_with_rand();
    weights_.insert(it, current_weights);
  }
}

void MatrixModel::ParseString(std::string strData) {
  if (strData.empty()) {
    throw std::out_of_range("Empty data");
  }
  const char separator = ',';
  std::stringstream streamData(strData);
  std::string val;
  int index = -1;
  while (std::getline(streamData, val, separator)) {
    if (index > kInputNeuronsNumber - 1) {
      throw std::out_of_range("Numbers are more than necessary");
    }
    if (-1 == index) {
      correct_result_ = atoi(val.c_str());
      if (correct_result_ > 26 || correct_result_ < 1) {
        throw std::out_of_range("There is no such letter");
      }
      OutputMatrix = S21Matrix(kOutputNeuronsNumber, 1);
      OutputMatrix(correct_result_ - 1, 0) = 1;
    } else {
      neurons_[0](index, 0) = atoi(val.c_str()) / 255.0;
    }
    index++;
  }
  if (index != kInputNeuronsNumber) {
    throw std::out_of_range("Numbers aren`t correct");
  }
}

/**
 * @brief Основная функция обучения сети. Ходит по эпохам и датасету и управляет
 * моделью
 *
 * @param path  Путь к файлу с датасетом ()
 */
void MatrixModel::StartProcessing() {
  std::string current_string;
  lost_.clear();
  InitLearnStep();
  for (int i = 0; i < number_of_epochs_; i++) {
    std::ifstream file(path_to_train_data);
    if (!file.is_open()) {
      throw std::out_of_range("Couldn't open train file in matrix model");
    }
    number_of_training = CalculatingDatasetVolume(path_to_train_data);
    while (getline(file, current_string)) {
      TrainModel(current_string);
    }
    if (i > 1) {
      learn_step /= 2;
    }
    file.close();
    TestMode();
    lost_.push_back(precision_of_prediction);
  }
  // wrw.WriteFile("output.txt", weights_);
}

/**
 * @brief Функция обучения. Один цикл.
 * 1)Проход вперед
 * 2)Подсчет ошибки
 * 3)Распространение обики назад
 *
 * @param source_data строка - значение нейронов. (закодированная буква)
 */
void MatrixModel::TrainModel(const std::string& source_data) {
  ParseString(source_data);
  MoveForward();
  LossFunction();
  CalcWeightsDelta(number_of_hidden_layers_);
  UpdateWeights(number_of_hidden_layers_);
  for (int i = number_of_hidden_layers_ - 1; i > 0; i--) {
    CalcError(i);
    CalcWeightsDelta(i);
    UpdateWeights(i);
  }
}

void MatrixModel::CalcError(int layer) {
  neuron_err_[layer] = weights_[layer].transpose() * weights_delta_[layer + 1];
}

void MatrixModel::UpdateWeights(int layer) {
  S21Matrix tmp(neurons_[layer - 1] * weights_delta_[layer].transpose());
  weights_[layer - 1] -= tmp.transpose() * learn_step;
}

void MatrixModel::CalcWeightsDelta(int layer) {
  for (int i = 0; i < NumberOfNeuronsInLayer[layer]; i++) {
    weights_delta_[layer](i, 0) =
        neuron_err_[layer](i, 0) * GetDerivative(neurons_[layer](i, 0));
  }
}

double MatrixModel::GetDerivative(double value) {
  return value * (1.0 - value);
}

/**
 * @brief функция потерь работает через сред. ошибку
 *
 */
void MatrixModel::LossFunction() {
  S21Matrix squared_errors(kOutputNeuronsNumber, 1);
  squared_errors = OutputMatrixLost.DerivativeMSE(OutputMatrix);
  neuron_err_ = neurons_;
  weights_delta_ = neurons_;
  neuron_err_[number_of_hidden_layers_] = squared_errors;
}

void MatrixModel::MoveForward() {
  for (int i = 1; i <= number_of_hidden_layers_; i++) {
    neurons_[i] = ((weights_[i - 1] * neurons_[i - 1]));
    ActivateFunction(i);
  }
  OutputMatrixLost = neurons_[number_of_hidden_layers_];
}

void MatrixModel::ActivateFunction(int current_layer) {
  for (int i = 0; i < NumberOfNeuronsInLayer[current_layer]; i++) {
    neurons_[current_layer](i, 0) =
        NormalizeWithSigmoid(neurons_[current_layer](i, 0));
  }
}

double MatrixModel::GetPrecisionOfPrediction() {
  return precision_of_prediction;
}

int MatrixModel::GetNumberOfTraining() { return number_of_training; }

double MatrixModel::NormalizeWithSigmoid(double value) {
  return 1 / (1 + pow(M_E, -value));
}

double MatrixModel::GetAverageAccuracy() { return average_accuracy; }

void MatrixModel::TestMode() {
  average_accuracy = 0;
  std::string current_string;
  std::ifstream file(path_to_test_data);
  if (!file.is_open()) {
    throw std::out_of_range("Сouldn't open test file in matrix model");
  }
  number_of_testing = CalculatingDatasetVolume(path_to_test_data) * part_of_set;
  correctly_predicted = 0;  // setter
  unsigned int start_time = clock();

  for (int index = number_of_testing; index > 0; index--) {
    getline(file, current_string);
    PredictFromString(current_string);
    average_accuracy += OutputMatrixLost(correct_result_ - 1, 0);
  }
  average_accuracy /= number_of_testing;
  unsigned int end_time = clock();
  run_time = (end_time - start_time) / double(1000 * 1000);  // время в секундах
  precision_of_prediction =
      (double(correctly_predicted) / double(number_of_testing + 1)) * 100;
}

char MatrixModel::CompareResult() {
  int index = 0;
  double value = OutputMatrixLost(0, 0);
  for (int i = 0; i < kOutputNeuronsNumber; i++) {
    double tmp = OutputMatrixLost(i, 0);
    if (tmp > value) {
      index = i;
      value = OutputMatrixLost(i, 0);
    }
  }
  if (index == correct_result_ - 1) correctly_predicted += 1;

  return char(index + 65);
}

char MatrixModel::PredictFromString(const std::string& source_data) {
  ParseString(source_data);
  MoveForward();
  return CompareResult();
}

char MatrixModel::Predict(std::vector<int> source_data) {
  int index = 0;
  for (std::vector<int>::iterator iter = source_data.begin();
       iter != source_data.end(); ++iter, index++) {
    neurons_[0](index, 0) = *iter > 230 ? 0 : 1;
  }
  MoveForward();
  return CompareResult();
}

void MatrixModel::LoadWeights(const std::string& path) {
  wrw.ReadFile(path, this->weights_);
  number_of_hidden_layers_ = this->weights_.size();
  NumberOfNeuronsInLayer.clear();
  SetNumberOfNeuronsInLayer(number_of_hidden_layers_);
  InitNeurons();
}

void MatrixModel::SaveWeights(const std::string& path) {
  wrw.WriteFile(path, this->weights_);
}

void MatrixModel::CrossValid() {
  std::string result = "";
  int index = group_crossvalid;
  SetEpochs(1);
  while (index > 0) {
    FilePrep(index);
    StartProcessing();
    TestMode();
    result += std::to_string(group_crossvalid - index + 1) + ": " +
              std::to_string(precision_of_prediction) + "%\n";
    index -= 1;
  }
  system(("rm " + static_cast<std::string>("kNameLearn.txt")).c_str());
  system(("rm " + static_cast<std::string>("kNameTest.txt")).c_str());

  SetPathToData(path_to_crossvalid, 0);
  SetPathToData("", 1);
  cross_valid_out = result;
  lost_.clear();
}

std::string MatrixModel::GetCrossValidOut() { return cross_valid_out; }

/**
 * @brief сеттер на пути к датасету
 *
 * @param path путь к файлу / по умолчанию пустая строка
 * @param mode 0 - установить путь к обучающей выбрке
 *             1 - установить путь к тестовой выборке
 */
void MatrixModel::SetPathToData(const std::string& path, int mode) {
  switch (mode) {
    case 0:
      path_to_train_data = path;
      break;
    case 1:
      path_to_test_data = path;
      break;
    default:
      throw std::out_of_range("unknown mode selected");
      break;
  }
}

std::string MatrixModel::GetPathToDataTrain() { return path_to_train_data; }

std::string MatrixModel::GetPathToDataTest() { return path_to_test_data; }

}  // namespace s21
