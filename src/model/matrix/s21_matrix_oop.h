#ifndef SRC_MAZE_MODEL_MATRIX_S21_MATRIX_OOP_H_
#define SRC_MAZE_MODEL_MATRIX_S21_MATRIX_OOP_H_

#include <cmath>
#include <cstring>
#include <iostream>

using std::string;
namespace s21 {
class S21Matrix {
 private:
  int _rows, _cols;
  double** _matrix;

 public:
  S21Matrix();
  S21Matrix(int rows, int cols);
  S21Matrix(const S21Matrix& other);
  S21Matrix(S21Matrix&& other);
  ~S21Matrix();

  bool eq_matrix(const S21Matrix& other);
  void sum_matrix(const S21Matrix& other);
  void sub_matrix(const S21Matrix& other);
  void mul_number(const double num);
  void mul_matrix(const S21Matrix& other);
  void fill_with_rand();
  S21Matrix transpose();
  S21Matrix calc_complements();
  double determinant();
  S21Matrix inverse_matrix();
  S21Matrix DerivativeMSE(S21Matrix& other);
  S21Matrix MSE(S21Matrix& other);
  double SumError();
  S21Matrix Log();


  S21Matrix operator+(const S21Matrix& other);
  S21Matrix operator-(const S21Matrix& other);
  S21Matrix operator*(const S21Matrix& other);
  friend S21Matrix operator*(const S21Matrix&, double);
  friend S21Matrix operator*(double, const S21Matrix&);
  bool operator==(const S21Matrix& other);
  void operator=(const S21Matrix& other);
  void operator+=(const S21Matrix& other);
  void operator-=(const S21Matrix& other);
  void operator*=(const S21Matrix& other);
  void operator*=(const double num);
  double& operator()(int i, int j);

  int get_rows();
  int get_cols();
  void set_rows(int);
  void set_cols(int);

  double** get_matrix();

  void printM() {
    std::cout << "Rows: " << _rows << std::endl;
    std::cout << "Cols: " << _cols << std::endl;
    for (int i = 0; i < _rows; i++) {
      for (int j = 0; j < _cols; j++) {
        std::cout << _matrix[i][j] << " ";
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  };

 protected:
  void allocate_memory();
  void calculate(const S21Matrix& other, char operation);
  double cofactor(int row, int column);
  S21Matrix get_reduced_for_minor(int row, int column);
  void copy_matrix(const S21Matrix& other);
  static constexpr int kDefaultCols = 3;
  static constexpr int kDefaultRows = 3;
  static constexpr double kEpsilon = 1e-07;
};

S21Matrix operator*(const S21Matrix&, double);
S21Matrix operator*(double, const S21Matrix&);
}  // namespace s21
#endif  // SRC_MAZE_MODEL_MATRIX_S21_MATRIX_OOP_H_
