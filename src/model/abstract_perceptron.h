#pragma once
#include <algorithm>
#include <array>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "base.h"
namespace s21 {

class BaseMLP : public Base {
 public:
  BaseMLP(/* args */);
  ~BaseMLP();
  std::string path_to_train_data = "temp_learn.csv";
  std::string path_to_test_data = "temp_test.csv";
  std::string path_to_crossvalid = "";
  std::string cross_valid_out = "";
  double run_time = 0;
  double part_of_set = 1;
  int group_crossvalid = 0;  // группы для кросс валидации
  int number_of_epochs_ = 0;
  std::vector<double> lost_;
  std::string GetCrossvalidOut();
  std::vector<double> GetLost();
  int CalculatingDatasetVolume(const std::string& path);
  void InitLearnStep();
  void SetEpochs(int value);
  void FilePrep(size_t count);
  void SetPartOfSet(double value);
  void SetPathToCrossvalid(const std::string& path);
  void SetPathToData(const std::string& path, int mode);
  void SetGroupCrossValid(int value);
  double GetRunTime();
};

}  // namespace s21
