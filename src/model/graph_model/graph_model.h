#ifndef SRC_MODEL_GRAPH_MODEL_S21_GRAPH_MODEL_H_
#define SRC_MODEL_GRAPH_MODEL_S21_GRAPH_MODEL_H_

#include "../abstract_perceptron.h"

namespace s21 {

typedef struct {
  int error;
  int correct;
  double average_accuracy;
  double cross_value;
} Parametrs;

class GraphLayer;
class GraphPreceptron;

// класс нейрона
class GraphNeuron : public Base {
  friend GraphLayer;

 private:
  double value_ = 0;          // значение нейрона
  double error_ = 0;          // ошибка нейрона
  double weights_delta_ = 0;  // разница весов нейрона
  std::vector<GraphNeuron*>
      p_layer_;  // вектор указателей на нейроны предыдущего слоя
  std::vector<GraphNeuron*>
      n_layer_;  // вектор указателей на нейроны следующего слоя
  std::vector<double> weights_;  // вектор весов

 public:
  // мутаторы
  void SetPLayer(GraphNeuron* prev);
  void SetNLayer(GraphNeuron* next);
  void InitWeights(double src);
  void SetValue(double src);
  void SetError(double src);
  // ассессоры
  double GetValueNeuron();
  // калькуляция
  void ActivateFunction();
  void CalcWeightsDelta();
  void UpdateWeights();
  void CalcError(size_t num);
  // работа с весами
  void SaveWeights(std::ofstream* file);
  void LoadWeights(const std::string& src);
  std::vector<double> GetWeights();
};

// класс слоя
class GraphLayer : public Base {
  friend GraphPreceptron;

 private:
  std::vector<GraphNeuron> neurons_;  // вектор нейронов

 public:
  void InitGraphLayer(size_t size);
  void SetLinks(GraphLayer* prev, GraphLayer* next);
  void SetValues(const income_t& income);
  void InitWeights(size_t size);
  void CalcValues();
  void CalcError(const result_t& result);
  void CalcWeightsDelta();
  void UpdateWeights();
  void CalcErrors();
  void SaveWeights(std::ofstream* file);
  void LoadWeights(std::ifstream* file);
};

// класс прецептрона
class GraphPreceptron : public BaseMLP {
  friend GraphNeuron;

 private:
  int correct_result_ = -1;
  size_t number_of_hidden_layers_ = 4;  // кол-во скрытых слоёв
  GraphLayer input_layer_;              // входной слой
  std::vector<GraphLayer> hidden_layers_;  // вектор скрытых слоёв
  GraphLayer output_layer_;                // выходной слой
  result_t result_ = {0};                  // массив результата
  Parametrs test_parametr_ = {0, 0, 0.0,
                              0.0};  // структура с результатами тестирвоания

  void MoveForward();
  void ResultInit(size_t num);
  void Learn();
  income_t ParseString(std::string src);

 public:
  GraphPreceptron();
  ~GraphPreceptron() {}
  void InitGraphPreceptron();
  void SetNumberOfHiddenLayers(size_t size);
  size_t GetNumberOfHiddenLayers();
  void SetInputLayer(income_t income);
  void LearnFromFile();
  size_t Predict();
  void TestFromFile();
  void SaveWeights(std::string name);
  bool LoadWeights(std::string name);
  std::string CrossValid();
  Parametrs GetParametrs();
  void InitWeights();
  void InitLayers();
  void LinkLayers();
  // void SetGroupCrossValid(int value);
};

}  // namespace s21

#endif  // SRC_MODEL_GRAPH_MODEL_S21_GRAPH_MODEL_H_
