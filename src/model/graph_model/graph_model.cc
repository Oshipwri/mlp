#include "./graph_model.h"

namespace s21 {
void GraphNeuron::SetPLayer(GraphNeuron *prev) { p_layer_.push_back(prev); }

void GraphNeuron::SetNLayer(GraphNeuron *next) { n_layer_.push_back(next); }

void GraphNeuron::InitWeights(double src) { weights_.push_back(src); }

void GraphNeuron::SetValue(double src) { value_ = src; }

void GraphNeuron::SetError(double src) { error_ = src; }

double GraphNeuron::GetValueNeuron() { return value_; }

std::vector<double> GraphNeuron::GetWeights() { return weights_; }

void GraphNeuron::ActivateFunction() {
  size_t index = 0;
  value_ = 0;
  while (index < weights_.size()) {
    value_ += weights_[index] * (p_layer_[index])->GetValueNeuron();
    index += 1;
  }

  value_ = 1.0 / (1.0 + pow(M_E, -value_));
}

void GraphNeuron::CalcWeightsDelta() {
  weights_delta_ = error_ * value_ * (1 - value_);
}

void GraphNeuron::UpdateWeights() {
  size_t index = 0;
  while (index < weights_.size()) {
    weights_[index] =
        weights_[index] - p_layer_[index]->value_ * weights_delta_ * learn_step;
    index += 1;
  }
}

void GraphNeuron::CalcError(size_t num) {
  size_t index = 0;
  error_ = 0;
  while (index < n_layer_.size()) {
    error_ += n_layer_[index]->weights_[num] * n_layer_[index]->weights_delta_;
    index += 1;
  }
}

void GraphNeuron::SaveWeights(std::ofstream *file) {
  size_t x = 0;
  while (x < weights_.size() - 1) {
    *file << weights_[x] << ' ';
    x += 1;
  }
  *file << weights_[x] << '\n';  // последний с переносом строки
}

void GraphNeuron::LoadWeights(const std::string &src) {
  std::istringstream s(src);
  size_t x = 0;
  std::string line;
  while (x < weights_.size() && std::getline(s, line, ' ')) {
    weights_[x] = std::stod(line);
    x += 1;
  }
}

void GraphLayer::InitGraphLayer(size_t size) {
  neurons_.clear();
  neurons_.resize(size);
}

void GraphLayer::InitWeights(size_t size) {
  size_t index = 0;
  while (index < neurons_.size()) {
    size_t y = 0;
    while (y < size) {
      neurons_[index].InitWeights(0.001 * (std::rand() % 2001 - 1000));
      y += 1;
    }
    index += 1;
  }
}

void GraphLayer::SetLinks(GraphLayer *prev, GraphLayer *next) {
  if (prev != nullptr) {
    size_t i = 0;
    while (i < prev->neurons_.size()) {
      size_t j = 0;
      while (j < neurons_.size()) {
        neurons_[j].SetPLayer(&(prev->neurons_[i]));
        j += 1;
      }
      i += 1;
    }
  }

  if (next != nullptr) {
    size_t i = 0;
    while (i < next->neurons_.size()) {
      size_t y = 0;
      while (y < neurons_.size()) {
        neurons_[y].SetNLayer(&((next->neurons_)[i]));
        y += 1;
      }
      i += 1;
    }
  }
}

void GraphLayer::SetValues(const income_t &income) {
  size_t index = 0;
  while (index < neurons_.size()) {
    neurons_[index].SetValue(income[index]);
    index += 1;
  }
}

void GraphLayer::CalcValues() {
  size_t index = 0;
  while (index < neurons_.size()) {
    neurons_[index].ActivateFunction();
    index += 1;
  }
}

void GraphLayer::CalcError(const result_t &result) {
  size_t index = 0;
  while (index < neurons_.size()) {
    neurons_[index].SetError(neurons_[index].value_ - result[index]);
    index += 1;
  }
}

void GraphLayer::CalcWeightsDelta() {
  size_t index = 0;
  while (index < neurons_.size()) {
    neurons_[index].CalcWeightsDelta();
    index += 1;
  }
}

void GraphLayer::UpdateWeights() {
  size_t index = 0;
  while (index < neurons_.size()) {
    neurons_[index].UpdateWeights();
    index += 1;
  }
}

void GraphLayer::CalcErrors() {
  size_t index = 0;
  while (index < neurons_.size()) {
    neurons_[index].CalcError(index);
    index += 1;
  }
}

void GraphLayer::SaveWeights(std::ofstream *file) {
  size_t x = 0;
  *file << neurons_[x].weights_.size() << std::endl;
  while (x < neurons_.size()) {
    neurons_[x].SaveWeights(file);
    x += 1;
  }
}

void GraphLayer::LoadWeights(std::ifstream *file) {
  size_t x = 0;
  std::string line;
  while (x < neurons_.size() && std::getline(*file, line, '\n')) {
    neurons_[x].LoadWeights(line);
    x += 1;
  }
}

GraphPreceptron::GraphPreceptron() { InitGraphPreceptron(); }

void GraphPreceptron::InitGraphPreceptron() {
  hidden_layers_.clear();
  InitLayers();
  LinkLayers();
  InitWeights();
}

void GraphPreceptron::LinkLayers() {
  input_layer_.SetLinks(nullptr, &(hidden_layers_[0]));

  hidden_layers_[0].SetLinks(&input_layer_, &(hidden_layers_[1]));
  int index = 1;
  while (index < number_of_hidden_layers_ - 1) {
    hidden_layers_[index].SetLinks(&(hidden_layers_[index - 1]),
                                   &(hidden_layers_[index + 1]));
    index += 1;
  }
  (hidden_layers_.back())
      .SetLinks(&(*(hidden_layers_.end() - 2)), &output_layer_);
  output_layer_.SetLinks(&(hidden_layers_.back()), nullptr);
}

void GraphPreceptron::InitWeights() {
  srand(time(NULL));
  hidden_layers_[0].InitWeights(kInputNeuronsNumber);
  int index = 1;
  while (index < number_of_hidden_layers_) {
    hidden_layers_[index].InitWeights(DefaultValueNeurons[index]);
    index += 1;
  }
  output_layer_.InitWeights(DefaultValueNeurons[index]);
}

void GraphPreceptron::InitLayers() {
  input_layer_.InitGraphLayer(DefaultValueNeurons[0]);

  if (number_of_hidden_layers_ > 5 || number_of_hidden_layers_ < 2) {
    throw std::out_of_range("Incorrect number of layers");
  }
  for (int i = 1; i <= number_of_hidden_layers_; i++) {
    GraphLayer temp;
    temp.InitGraphLayer(DefaultValueNeurons[i]);
    hidden_layers_.push_back(temp);
  }
  output_layer_.InitGraphLayer(
      DefaultValueNeurons[DefaultValueNeurons.size() - 1]);
}

void GraphPreceptron::SetNumberOfHiddenLayers(size_t value) {
  number_of_hidden_layers_ = value - 1;
  InitGraphPreceptron();
}

size_t GraphPreceptron::GetNumberOfHiddenLayers() {
  return number_of_hidden_layers_ + 1;
}

void GraphPreceptron::SetInputLayer(income_t value) {
  input_layer_.SetValues(value);
}

void GraphPreceptron::MoveForward() {
  size_t index = 0;
  while (index < number_of_hidden_layers_) {
    hidden_layers_[index].CalcValues();
    index += 1;
  }
  output_layer_.CalcValues();
}

// 1 эпоха обучения
void GraphPreceptron::Learn() {
  output_layer_.CalcError(result_);
  output_layer_.CalcWeightsDelta();
  output_layer_.UpdateWeights();

  for (int i = hidden_layers_.size() - 1; i >= 0; i--) {
    hidden_layers_[i].CalcErrors();
    hidden_layers_[i].CalcWeightsDelta();
    hidden_layers_[i].UpdateWeights();
  }
}

BaseMLP::income_t GraphPreceptron::ParseString(std::string src) {
  income_t result;
  std::istringstream s(src);
  size_t index = 0;
  std::string line;
  while (std::getline(s, line, ',')) {
    result[index] = std::stol(line) / 255.0;
    index += 1;
  }
  return result;
}

void GraphPreceptron::LearnFromFile() {
  lost_.clear();
  for (int i = 0; i < number_of_epochs_; i++) {
    std::ifstream file(path_to_train_data);
    if (!file.is_open()) {
      throw std::out_of_range("Сouldn't open train file in graph model");
    }
    std::string current_string;
    while (!file.eof()) {
      size_t result_num = 0;
      std::getline(file, current_string, ',');
      result_ = {0};
      result_[std::atoi(current_string.c_str()) - 1] =
          1;  // в массив результата вставляем правильный ответ
      std::getline(file, current_string, '\n');
      if (current_string.size() == 0) continue;
      income_t tmp = ParseString(current_string);
      SetInputLayer(tmp);
      MoveForward();
      Learn();
    }
    file.close();
    TestFromFile();
    lost_.push_back(test_parametr_.cross_value);
  }
}

size_t GraphPreceptron::Predict() {
  size_t result = 0;
  MoveForward();
  size_t x = 1;
  while (x < kOutputNeuronsNumber) {
    if (output_layer_.neurons_[x].GetValueNeuron() >
        output_layer_.neurons_[result].GetValueNeuron())
      result = x;
    x += 1;
  }
  return result;
}

void GraphPreceptron::TestFromFile() {
  std::ifstream file(path_to_test_data);
  if (!file.is_open()) {
    throw std::out_of_range("Сouldn't open test file in graph model");
  }

  int number_of_testing =
      CalculatingDatasetVolume(path_to_test_data) * part_of_set;
  int index = number_of_testing;
  std::string line;
  double accuracy = 0.0;
  int cal = 0;
  unsigned int start_time = clock();
  while (index--) {
    size_t element = 0;
    std::getline(file, line, ',');
    element = std::atoi(line.c_str()) - 1;
    std::getline(file, line, '\n');
    if (line.size() > 0) {
      SetInputLayer(ParseString(line));
      if (Predict() == element) {
        cal += 1;
      }
      accuracy += output_layer_.neurons_[element].GetValueNeuron();
    }
  }
  file.close();
  unsigned int end_time = clock();
  run_time = (end_time - start_time) / double(1000 * 1000);  // время в секундах
  test_parametr_.error = number_of_testing - cal;
  test_parametr_.correct = cal;
  if (number_of_testing != 0) {
    test_parametr_.average_accuracy = accuracy / number_of_testing;
    test_parametr_.cross_value = double(cal) / number_of_testing * 100.0;
  } else {
    test_parametr_.average_accuracy = 0.0;
    test_parametr_.cross_value = 0.0;
  }
}

void GraphPreceptron::SaveWeights(std::string name) {
  std::ofstream file(name);
  if (!file.is_open()) throw std::out_of_range("File wasn't opened.");
  size_t index = 0;
  file << number_of_hidden_layers_ + 1 << std::endl;
  while (index < number_of_hidden_layers_) {
    file << hidden_layers_[index].neurons_.size() << " ";
    hidden_layers_[index].SaveWeights(&file);
    index += 1;
    file << std::endl;
  }
  file << output_layer_.neurons_.size() << " ";
  output_layer_.SaveWeights(&file);
  file.close();
}

bool GraphPreceptron::LoadWeights(std::string name) {
  bool result = false;
  std::string line;
  std::ifstream file(name);
  if (!file.is_open()) throw std::out_of_range("File wasn't opened.");
  result = true;
  size_t x = 0;
  int current;
  file >> current;
  if (current < 2 || current > 5)
    throw std::out_of_range("Incorrect layers number");
  SetNumberOfHiddenLayers(current);
  while (x < number_of_hidden_layers_) {
    file >> current;
    if (current != hidden_layers_[x].neurons_.size())
      throw std::out_of_range("Incorrect neurons size in file");
    file >> current;
    if (current != hidden_layers_[x].neurons_[0].GetWeights().size())
      throw std::out_of_range("Incorrect weights size in file");
    std::getline(file, line, '\n');
    hidden_layers_[x].LoadWeights(&file);
    x += 1;
  }
  file >> current;
  if (current != output_layer_.neurons_.size())
    throw std::out_of_range("Incorrect neurons size in file");
  file >> current;
  if (current != output_layer_.neurons_[0].GetWeights().size())
    throw std::out_of_range("Incorrect weights size in file");
  std::getline(file, line, '\n');
  output_layer_.LoadWeights(&file);
  file.close();
  return result;
}

std::string GraphPreceptron::CrossValid() {
  std::string result = "";
  int index = group_crossvalid;
  number_of_epochs_ = 1;
  while (index > 0) {
    FilePrep(index);
    LearnFromFile();
    TestFromFile();
    std::string temp = std::to_string(test_parametr_.cross_value);
    result += std::to_string(group_crossvalid - index + 1) + ": " +
              temp.substr(0, temp.find(".") + 3) + "%\n";
    index -= 1;
  }

  system(("rm " + static_cast<std::string>("kNameLearn.txt")).c_str());
  system(("rm " + static_cast<std::string>("kNameTest.txt")).c_str());

  SetPathToData(path_to_crossvalid, 0);
  SetPathToData("", 1);
  cross_valid_out = result;
  return result;
}

Parametrs GraphPreceptron::GetParametrs() { return test_parametr_; }

}  // namespace s21
