#include "./model.h"

namespace s21 {

Model::Model() : matrix_strategy_(this), graph_strategy_(this) {
  strategy_ = &matrix_strategy_;
}

void Model::Learn() { strategy_->Learn(); }
void Model::MatrixModelLearn() { matrix_model_.StartProcessing(); }
void Model::GraphModelLearn() { graph_model_.LearnFromFile(); }

void Model::MakeTest() { strategy_->MakeTest(); }
void Model::MatrixModelMakeTest() { matrix_model_.TestMode(); }
void Model::GraphModelMakeTest() { graph_model_.TestFromFile(); }

void Model::CrossValid() { strategy_->CrossValid(); }
void Model::MatrixModelCrossValid() { matrix_model_.CrossValid(); }
void Model::GraphModelCrossValid() { graph_model_.CrossValid(); }

void Model::SetMatrixStrategy() { strategy_ = &matrix_strategy_; }

void Model::SetGraphStrategy() { strategy_ = &graph_strategy_; }

char Model::Predict(std::vector<int>& neurons) {
  return strategy_->Predict(neurons);
}
char Model::MatrixModelPredict(const std::vector<int>& neurons) {
  return matrix_model_.Predict(neurons);
}
char Model::GraphModelPredict(const std::vector<int>& neurons) {
  GraphPreceptron::income_t temp;
  for (int i = 0; i < neurons.size(); i++) temp[i] = neurons[i] > 230 ? 0 : 1;
  graph_model_.SetInputLayer(temp);
  return graph_model_.Predict() + 65;
}

int Model::GetNumberOfHiddenLayers() {
  return strategy_->GetNumberOfHiddenLayers();
}
int Model::GetNumberOfHiddenLayersInMatrixModel() {
  return matrix_model_.GetNumberOfHiddenLayers();
}
int Model::GetNumberOfHiddenLayersInGraphModel() {
  return graph_model_.GetNumberOfHiddenLayers();
}

void Model::SetNumberOfHiddenLayers(int layers_number) {
  strategy_->SetNumberOfHiddenLayers(layers_number);
}
void Model::SetNumberOfHiddenLayersInMatrixModel(int layers_number) {
  matrix_model_.SetNumberOfHiddenLayers(layers_number);
}
void Model::SetNumberOfHiddenLayersInGraphModel(int layers_number) {
  graph_model_.SetNumberOfHiddenLayers(layers_number);
}

void Model::SetEpochs(int epochs_number) {
  strategy_->SetEpochs(epochs_number);
}
void Model::SetEpochsInMatrixModel(int epochs_number) {
  matrix_model_.SetEpochs(epochs_number);
}
void Model::SetEpochsInGraphModel(int epochs_number) {
  graph_model_.SetEpochs(epochs_number);
}

void Model::SetGroupsNumber(int groups_number) {
  strategy_->SetGroups(groups_number);
}
void Model::SetGroupsInMatrixModel(int groups_number) {
  matrix_model_.SetGroupCrossValid(groups_number);
}
void Model::SetGroupsInGraphModel(int groups_number) {
  graph_model_.SetGroupCrossValid(groups_number);
}

void Model::LoadWeights(const std::string& path) {
  matrix_model_.LoadWeights(path);
  graph_model_.LoadWeights(path);
}

void Model::SaveWeights(const std::string& path) {
  strategy_->SaveWeights(path);
}

void Model::SaveWeightsInMatrixModel(const std::string& path) {
  matrix_model_.SaveWeights(path);
}
void Model::SaveWeightsInGraphModel(const std::string& path) {
  graph_model_.SaveWeights(path);
}

void Model::SetLearnFile(const std::string& path) {
  matrix_model_.SetPathToData(path, 0);
  graph_model_.SetPathToData(path, 0);
}

void Model::SetTestFile(const std::string& path) {
  matrix_model_.SetPathToData(path, 1);
  graph_model_.SetPathToData(path, 1);
}

void Model::SetTestFilePercent(double percent) {
  matrix_model_.SetPartOfSet(percent);
  graph_model_.SetPartOfSet(percent);
}

double Model::GetAccuracy() { return strategy_->GetAccuracy(); }
double Model::GetAccuracyInMatrixModel() {
  return matrix_model_.GetAverageAccuracy();
}
double Model::GetAccuracyInGraphModel() {
  return graph_model_.GetParametrs().average_accuracy;
}

double Model::GetTestTime() { return strategy_->GetTestTime(); }
double Model::GetTestTimeInMatrixModel() { return matrix_model_.GetRunTime(); }
double Model::GetTestTimeInGraphModel() { return graph_model_.GetRunTime(); }

double Model::GetPrecision() { return strategy_->GetPrecision(); }
double Model::GetPrecisionInMatrixModel() {
  return matrix_model_.GetPrecisionOfPrediction();
}
double Model::GetPrecisionInGraphModel() {
  return graph_model_.GetParametrs().cross_value;
}

std::string Model::GetPathToLearnFile() {
  return matrix_model_.GetPathToDataTrain();
}

void Model::SetCrossValidLearnFileName(const std::string& path) {
  strategy_->SetCrossValidLearnFileName(path);
}
void Model::SetCrossValidLearnFileInMatrixModel(const std::string& path) {
  matrix_model_.SetPathToCrossvalid(path);
}
void Model::SetCrossValidLearnFileInGraphModel(const std::string& path) {
  graph_model_.SetPathToCrossvalid(path);
}

std::string Model::GetCrossValidOutput() {
  return strategy_->GetCrossValidOutput();
}
std::string Model::GetCrossValidOutputInMatrixModel() {
  return matrix_model_.GetCrossValidOut();
}
std::string Model::GetCrossValidOutputInGraphModel() {
  return graph_model_.GetCrossvalidOut();
}

std::vector<double> Model::GetErrorData() { return strategy_->GetError(); }

std::vector<double> Model::GetErrorDataInMatrixModel() {
  return matrix_model_.GetLost();
}
std::vector<double> Model::GetErrorDataInGraphModel() {
  return graph_model_.GetLost();
}

void MatrixStrategy::Learn() { model_->MatrixModelLearn(); }
void GraphStrategy::Learn() { model_->GraphModelLearn(); }

void MatrixStrategy::MakeTest() { model_->MatrixModelMakeTest(); }
void GraphStrategy::MakeTest() { model_->GraphModelMakeTest(); }

void MatrixStrategy::CrossValid() { model_->MatrixModelCrossValid(); }
void GraphStrategy::CrossValid() { model_->GraphModelCrossValid(); }

char MatrixStrategy::Predict(std::vector<int>& neurons) {
  return model_->MatrixModelPredict(neurons);
}
char GraphStrategy::Predict(std::vector<int>& neurons) {
  return model_->GraphModelPredict(neurons);
}

int MatrixStrategy::GetNumberOfHiddenLayers() {
  return model_->GetNumberOfHiddenLayersInMatrixModel();
}
int GraphStrategy::GetNumberOfHiddenLayers() {
  return model_->GetNumberOfHiddenLayersInGraphModel();
}

void MatrixStrategy::SetNumberOfHiddenLayers(int layers_number) {
  model_->SetNumberOfHiddenLayersInMatrixModel(layers_number);
}
void GraphStrategy::SetNumberOfHiddenLayers(int layers_number) {
  model_->SetNumberOfHiddenLayersInGraphModel(layers_number);
}

void MatrixStrategy::SetEpochs(int epochs_number) {
  model_->SetEpochsInMatrixModel(epochs_number);
}
void GraphStrategy::SetEpochs(int epochs_number) {
  model_->SetEpochsInGraphModel(epochs_number);
}

void MatrixStrategy::SetGroups(int groups_number) {
  model_->SetGroupsInMatrixModel(groups_number);
}
void GraphStrategy::SetGroups(int groups_number) {
  model_->SetGroupsInGraphModel(groups_number);
}

void MatrixStrategy::SaveWeights(const std::string& path) {
  model_->SaveWeightsInMatrixModel(path);
}
void GraphStrategy::SaveWeights(const std::string& path) {
  model_->SaveWeightsInGraphModel(path);
}

double MatrixStrategy::GetAccuracy() {
  return model_->GetAccuracyInMatrixModel();
}
double GraphStrategy::GetAccuracy() {
  return model_->GetAccuracyInGraphModel();
}

double MatrixStrategy::GetTestTime() {
  return model_->GetTestTimeInMatrixModel();
}
double GraphStrategy::GetTestTime() {
  return model_->GetTestTimeInGraphModel();
}

double MatrixStrategy::GetPrecision() {
  return model_->GetPrecisionInMatrixModel();
}

double GraphStrategy::GetPrecision() {
  return model_->GetPrecisionInGraphModel();
}

void MatrixStrategy::SetCrossValidLearnFileName(const std::string& path) {
  model_->SetCrossValidLearnFileInMatrixModel(path);
}
void GraphStrategy::SetCrossValidLearnFileName(const std::string& path) {
  model_->SetCrossValidLearnFileInGraphModel(path);
}

std::string MatrixStrategy::GetCrossValidOutput() {
  return model_->GetCrossValidOutputInMatrixModel();
}
std::string GraphStrategy::GetCrossValidOutput() {
  return model_->GetCrossValidOutputInGraphModel();
}

std::vector<double> MatrixStrategy::GetError() {
  return model_->GetErrorDataInMatrixModel();
}
std::vector<double> GraphStrategy::GetError() {
  return model_->GetErrorDataInGraphModel();
}

MatrixStrategy::~MatrixStrategy() {}
GraphStrategy::~GraphStrategy() {}
}  // namespace s21
