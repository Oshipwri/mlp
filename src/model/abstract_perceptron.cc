#include "abstract_perceptron.h"

namespace s21 {

BaseMLP::BaseMLP(/* args */) {}

BaseMLP::~BaseMLP() {}

void BaseMLP::InitLearnStep() { learn_step = 0.1; }

void BaseMLP::SetPartOfSet(double value) {
  if (value < 0 || value > 1) {
    throw std::out_of_range(
        "value of "
        "part_of_set"
        " isn`t valid");
  }
  part_of_set = value;
}

void BaseMLP::FilePrep(size_t count) {
  int str_count = CalculatingDatasetVolume(path_to_crossvalid);
  size_t str_test = str_count / group_crossvalid;
  std::ofstream file_cross_learn("kNameLearn.txt");
  std::ofstream file_test("kNameTest.txt");
  SetPathToData("kNameLearn.txt", 0);
  SetPathToData("kNameTest.txt", 1);

  std::string current_string;
  std::ifstream file(path_to_crossvalid);  // файл из которого читаем
  if (!file.is_open()) {
    throw std::out_of_range("string isn`t correct");
  }
  int x = 0;
  while (getline(file, current_string)) {
    if (x < str_test * (count - 1) || x >= str_test * count) {
      file_cross_learn << current_string << '\n';
    } else {
      file_test << current_string << '\n';
    }
    x++;
  }
  file.close();
  file_cross_learn.close();
  file_test.close();
}

int BaseMLP::CalculatingDatasetVolume(const std::string& path) {
  std::ifstream inFile(path);
  return std::count(std::istreambuf_iterator<char>(inFile),
                    std::istreambuf_iterator<char>(), '\n');
}

void BaseMLP::SetPathToData(const std::string& path, int mode) {
  switch (mode) {
    case 0:
      path_to_train_data = path;
      break;
    case 1:
      path_to_test_data = path;
      break;
    default:
      throw std::out_of_range("unknown mode selected");
      break;
  }
}

void BaseMLP::SetPathToCrossvalid(const std::string& path) {
  path_to_crossvalid = path;
}

void BaseMLP::SetEpochs(int value) { number_of_epochs_ = value; }

std::string BaseMLP::GetCrossvalidOut() { return cross_valid_out; }

std::vector<double> BaseMLP::GetLost() { return lost_; }

void BaseMLP::SetGroupCrossValid(int value) { group_crossvalid = value; }

double BaseMLP::GetRunTime() { return run_time; }

}  // namespace s21
