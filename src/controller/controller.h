#ifndef SRC_CONTROLLER_CONTROLLER_H
#define SRC_CONTROLLER_CONTROLLER_H

#include "../model/model.h"

namespace s21 {
class Controller {
 public:
  explicit Controller(s21::Model *model) : model_(model){};
  ~Controller() {}
  Controller(const Controller &other) = default;
  Controller(Controller &&other) = default;
  Controller &operator=(const Controller &other) = delete;
  Controller &operator=(Controller &&other) = delete;

  void SetMatrixMethod() { model_->SetMatrixStrategy(); }
  void SetGraphMethod() { model_->SetGraphStrategy(); }

  void LearnRandomWeights() { model_->Learn(); }
  void CrossValid() { model_->CrossValid(); }
  void MakeTest() { model_->MakeTest(); }
  char Predict(std::vector<int> &neurons) { return model_->Predict(neurons); }

  void LoadWeights(const std::string &path) { model_->LoadWeights(path); }
  void SaveWeights(const std::string &path) { model_->SaveWeights(path); }
  int GetNumberOfHiddenLayers() { return model_->GetNumberOfHiddenLayers(); }
  void SetNumberOfHiddenLayers(int layers_number) {
    model_->SetNumberOfHiddenLayers(layers_number);
  }
  void SetLearnFile(const std::string &path) { model_->SetLearnFile(path); }
  void SetTestFile(const std::string &path) { model_->SetTestFile(path); }
  void SetTestFilePercent(double percent) {
    model_->SetTestFilePercent(percent);
  }
  void SetGroupsNumber(int groups_number) {
    model_->SetGroupsNumber(groups_number);
  }

  double GetAccuracy() { return model_->GetAccuracy(); }
  double GetTestTime() { return model_->GetTestTime(); }
  double GetPrecision() { return model_->GetPrecision(); }

  void SetEpochs(int epochs_number) { model_->SetEpochs(epochs_number); }

  std::string GetPathToLearnFile() { return model_->GetPathToLearnFile(); }
  std::string GetCrossValidOutput() { return model_->GetCrossValidOutput(); }

  void SetCrossValidLearnFileName(const std::string &path) {
    model_->SetCrossValidLearnFileName(path);
  }

  std::vector<double> GetErrorData() { return model_->GetErrorData(); }

 private:
  s21::Model *model_;
};

}  // namespace s21

#endif  // SRC_CONTROLLER_CONTROLLER_H
